# Create nic
module "nic" {
  source               = "git::https://gitlab.com/azure-iac2/root-modules.git//nic"
  nic_name             = var.nic_name  
  business_divsion     = var.business_divsion
  environment          = var.environment
  resource_group_name  = data.azurerm_resource_group.rg.name
  location             = data.azurerm_resource_group.rg.location
  nic_ip_name          = var.nic_ip_name
  subnet_id            = data.azurerm_subnet.gl-subnet.id
  private_ip_address_allocation = var.private_ip_address_allocation
  public_ip_address_id = data.azurerm_public_ip.gl-public-ip.id
}