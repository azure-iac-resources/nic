# Generic Input Variables
# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type = string
  default = "gitlab"
}
# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type = string
  default = "dev"
}

# Azure Resource Group Name 
variable "resource_group_name" {
  description = "Resource Group Name"
  type = string
  default = "default-rg"
}

variable "location" {
  description = "Resource Group location"
  type = string
  default = "default"
}

variable "virtual_network_name" {
  description = "virtual network name"
  type = string
  default = "default"
}

# Azure NIC Name 
variable "nic_name" {
  description = "NIC Name"
  type = string
  default = "default-nic"
}

variable "nic_ip_name" {
  description = "nic ip configuration name"
  type = string
  default = "default"
}

variable "subnet_name" {
  description = "Azure subnet name"
  type = string
  default = "default"
}

variable "private_ip_address_allocation" {
  description = "NIC private_ip_address_allocation"
  type = string
  default = "default"
}

variable "public_ip_name" {
  description = "NIC public_ip_name"
  type = string
  default = "default"
}