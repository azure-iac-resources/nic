data "azurerm_resource_group" "rg"{
  name = var.resource_group_name
}

data "azurerm_subnet" "gl-subnet" {
  name                 = var.subnet_name
  virtual_network_name = var.virtual_network_name
  resource_group_name  = data.azurerm_resource_group.rg.name
}

data "azurerm_public_ip" "gl-public-ip" {
  name                = var.public_ip_name
  resource_group_name = data.azurerm_resource_group.rg.name
}